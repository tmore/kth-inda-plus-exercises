	.text
	.global _start

sum:
	mov $0, %rax

loop:
	cmp $0, %rdi
	je loop_done

	add %rdi, %rax

	sub $1, %rdi
	jmp loop
loop_done:
	push $10
	ret

_start:

	call sum


	mov $60, %rax      # For sys_exit
	mov $0, %rdi       # Exit code 0
	syscall
