        .global _start


_start:

        push $10

        mov (%rsp), %rax
        movq $5, (%rsp)
        movq (%rsp), %rbi

        add %rbi, %rax

        sub $8, %rsp

        mov $60, %rax      # For sys_exit
        mov $0, %rdi       # Exit code 0
        syscall
