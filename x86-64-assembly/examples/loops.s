        .text
        .global _start

_start:

        mov $3, %rdi       # set %rdi to 3

        mov $0, %rax       # set %rax to 0

loop:
        cmp $0, %rdi       # compare contents of %rdi with 0
        je loop_done       # if they are equal jump to loop_done

        add %rdi, %rax     # set %rax = %rdi + %rax

        sub $1, %rdi       # set %rdi = %rdi - 1
        jmp loop           # unconditionally jumpy back to start of loop

loop_done:

        mov $60, %rax      # For sys_exit
        mov $0, %rdi       # Exit code 0
        syscall
