        .text
        .global _start

_start:


        mov $1000, %rax # Move the value of 1000 to %rax
        mov $337, %rbx  # Move the value of 337 to %rbx

        add %rbx, %rax  # 100


        mov $0, %rax    # %rax is 0
        mov $1, %rbx    # %rbx is 1
        sub %rbx, %rax  # 0 - 1 iiiis ?



        movb $255, %al  # One full byte into %rax
        addb $1, %al     # and adding by one gives...


        mov $60, %rax      # For sys_exit
        mov $0, %rdi       # Exit code 0
        syscall
