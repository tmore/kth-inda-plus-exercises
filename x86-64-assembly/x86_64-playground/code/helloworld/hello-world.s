        .text
        .global _start


_start:

        mov $1, %rax       # Puts 1 into the register eax, for sys_write
        mov $1, %rdi       # Use stdout
        mov $msg, %rsi     # Point to the start of our buffer
        mov len, %rdx      # we want to write 12 bytes
        syscall            # Actually performs the action


        mov $60, %rax      # For sys_exit
        mov $0, %rdi       # Exit code 0
        syscall

        mov $0, %rdx
        mov $5, %rax
        mov $2, %rsi
        div %rsi

        .data
msg:
        .string "Hello world!\0"
len:
        .int .-msg
