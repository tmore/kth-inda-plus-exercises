	.bss
	.lcomm str_buf 1024

	.data

newline_str:	.ascii "\n"

	.text
	.global int_to_str
	.global str_to_int
	.global print_str
	.global print_c_str
	.global print_newline
	.global read_buf
	.global read_int



	# %rdi should be singed int that we want to calculate the length for
	# %rax will be the number of digits required to encode the value of %rdi
int_str_length:
	mov %rdi, %rax              # Store the number in %rax
	mov $0, %rdi                # %rdi becomes the counter
	mov $10, %r11

int_str_length_loop:
	add $1, %rdi                # Every time this loop runs we add one to the count
	xor %rdx, %rdx              # Because how div works
	div %r11                     # Divide by ten
	cmp $0, %rax                # Compare the result with 0
	jne int_str_length_loop
	mov %rdi, %rax              # Move the result to %rax
	ret


	# %rdi - The integer to convert
	# %rsi - A pointer to a buffer where the number can be stored
	# %rdx - the size of said buffer
	# The number in %rdi will be converted to an ascii string representation
	# in the buffer in %rsi, if the number is to big for the
	# argument %rax will be zero. Else it will be the number of characters
	# necesary to encode the number
int_to_str:
	push %rdi              # Push data to the stack for function call
	push %rsi
	push %rdx

	call int_str_length # Calculates the final length of the number

	pop %rdx               # pops  memory back from the stack
	pop %rsi
	pop %rdi

	cmp %rdx, %rax          # First make sure that the buffer is big enough

	jg int_to_str_fail      # if the final length is greater then buffer size, fail

	mov %rsi, %r8       # Copy the bufer pointer to %r8,
			    # %r8 will be our current position pointer
	add %rax, %r8       # Since we write the number backwards, go to the end position

	mov %rdi, %rax      # Move the input number to %rax for arithmetics
	mov $0, %r9         # Counter of how many characters that has been written

int_to_str_loop:
	dec %r8              # Moves the pointer one step "forward"
	inc %r9              # Increases the count by 1
	mov $10, %r11         # Ten for base
	xor %rdx, %rdx       # Clearing the bits in rdx, the because of how div works.
	div %r11              # Divide the remaining number with 10 for bas 12 numbers
	add $0x30, %dl       # Add 0x30 to the remainder of the division
			     # this converts the remainder to ascii numbers
	movb %dl, (%r8)      # store this into the location of %r8
	cmp $0, %rax         # to see if the rest is zero

	jne int_to_str_loop  # if it isn't loop!

	sub $1024, %rbp      # Reset the stack pointer
	mov %r9, %rax        # Set the values to the proper registers
	ret
int_to_str_fail:
	mov $0, %rax
	ret

str_to_int:
	ret

	# Prints a string to stdout
	# string starting adress should be in %rdi
	# string end adress should be in %rsi
print_str:
	mov %rsi, %rdx     # Swaps the content of the buffers
	mov %rdi, %rsi

	mov $1, %rax       # Puts 1 into the register eax, for sys_write
	mov $1, %rdi       # Use stdout
	syscall
	ret

print_newline:
	mov $newline_str, %rdi
	mov $1, %rsi
	call print_str
	ret


	# Prints a c type string where the last element should be 0
	# expects %rdi to be a pointer to a c type string
print_c_str:
	ret


read_buf:
	ret
read_int:
	ret
