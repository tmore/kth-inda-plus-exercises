	.text
	.global list_cons
	.global list_head
	.global list_tail
	.global list_is_null
	.global list_get_null
	.global list_length

list_cons:
	ret
list_head:
	ret
list_tail:
	ret
list_is_null:
	ret

list_get_null:
	ret

list_length:
	ret


list_free_all:
	ret
