.data
msg:
	.ascii "Hello world!\n\0"
len:
	.int 15

	.text
	.global _start

replace_char:
	ret
_start:


	mov $1, %rax       # Puts 1 into the register eax, for sys_write
	mov $1, %rdi       # Use stdout
	mov $msg, %rsi     # Point to the start of our buffer
	mov len, %rdx      # we want to write 12 bytes
	syscall            # Actually performs the action

	mov $60, %rax      # For sys_exit
	mov $0, %rdi       # Exit code 0
	syscall
