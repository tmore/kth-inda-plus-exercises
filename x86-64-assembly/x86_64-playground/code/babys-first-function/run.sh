#!/bin/bash
as --gstabs+ bff.s -o bff.o
as --gstabs+ other.s -o other.o
as --gstabs+ ../util/string.s -o util-string.o

ld bff.o other.o util-string.o -o bff
./bff
