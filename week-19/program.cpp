#include "program.hpp"


Program::Program(Shader vertex, Shader fragment){
  program_id_= glCreateProgram();
  glAttachShader(program_id_, vertex.get_glid());
  glAttachShader(program_id_, fragment.get_glid());
  glLinkProgram(program_id_);
  GLint success;
  glGetProgramiv(program_id_, GL_LINK_STATUS, &success);
  if( !success ){
    linked_ = false;
    char info_log[1024];
    glGetProgramInfoLog(program_id_, 1024, NULL, info_log);
    error_msg_ = info_log;
    return;
  }
}


GLint Program::get_program(){
  return program_id_;
}

void Program::use(){
  glUseProgram(get_program());
}

bool Program::linked(){
  return linked_;
}

string Program::get_error_msg(){
  return error_msg_;
}
