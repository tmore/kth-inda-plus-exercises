#version 330 core

in vec4 vertex_color;
out vec4 color;
in float z;

void main(){
  color = vertex_color;
}
