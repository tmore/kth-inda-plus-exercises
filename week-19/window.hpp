#pragma once

#include <GLFW/glfw3.h>
#include "program.hpp"

class Window{
private:
  GLFWwindow* window_;
  Program program_;
  string error_msg_;
  bool all_ok_;
public:

  Window(string window_name, int width, int height);
  ~Window();

  void use_program(Program program);

  void run();
  bool all_ok();
  string get_error_msg();

};
