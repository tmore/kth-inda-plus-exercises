#pragma once

#include <string>
#include <GLFW/glfw3.h>

using namespace std;


class Shader{
private:
  GLuint shader_type_;
  bool   compiled_ = true;
  string error_msg_;
  GLuint shader_id_;
public:
  Shader(string shader_fp, GLuint shader_type);
  ~Shader();
  bool compiled();
  GLint get_glid();
  string get_error_msg();

};
