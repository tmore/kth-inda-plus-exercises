#define GLM_ENABLE_EXPERIMENTAL

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <string>
#include <iostream>
#include <cmath>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "shader.hpp"
#include "program.hpp"

#define WINDOW_WIDTH 1920
#define WINDOW_HEIGHT 1080

using namespace std;


GLfloat triangle_vertices[] = {
  0.0, 1.0,   0.0,
  -1.0, -1.0, 0.0,
  1.0, -1.0, 0.0
};

GLfloat triangle_colors[] = {
  1.0, 0.0,0.0,
  0.0,1.0,0.0,
  0.0,0.0,1.0
};

GLfloat cube_vertices[] = {
  -1.0, 1.0, 1.0,
   1.0, 1.0, 1.0,
   1.0, 1.0,-1.0,
  -1.0, 1.0,-1.0,
  -1.0,-1.0, 1.0,
   1.0,-1.0, 1.0,
   1.0,-1.0,-1.0,
  -1.0,-1.0,-1.0
};

GLuint cube_indices[] = {
  7, 3, 2,
  7, 2, 6,
  6, 2, 1,
  6, 1, 5,
  5, 1, 0,
  5, 0, 4,
  4, 0, 3,
  4, 3, 7,
  3, 0, 1,
  3, 1, 2,
  7, 5, 4,
  7, 6, 5
};


void error_callback(int error, const char* desctiption){
  cout << desctiption << endl;

}



void framebuffer_size_callback(GLFWwindow* window, int width, int height){
    glViewport(0, 0, width, height);
}

int main(int argc, char** argv){

  string vertex_fp, fragment_fp;
  if(argc >= 2){
    vertex_fp = argv[1];
  } else {
    vertex_fp = "default.vs";
  }
  if(argc >= 3){
    fragment_fp = argv[2];
  } else {
    fragment_fp = "default.fs";
  }



  glfwInit();
  glfwSetErrorCallback(error_callback);
  glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, 1);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  GLFWwindow* window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT,
                                        "Derpy derp", NULL, NULL);
  glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

  if (window == NULL){
    cerr << "Failed to create GLFW window" << endl;
    glfwTerminate();
    return 1;
  }
  glfwMakeContextCurrent(window);

  glewInit();


  glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

  Shader vertex(vertex_fp, GL_VERTEX_SHADER);

  if(!vertex.compiled()){
    cerr << "Vertex shader failed to compile:" << endl
         << vertex.get_error_msg() << endl;
    return 1;
  }
  Shader fragment(fragment_fp, GL_FRAGMENT_SHADER);
  if(!fragment.compiled()){
    cerr << "Fragment shader failed to compile" << endl
         << fragment.get_error_msg() << endl;
    return 1;
  }

  Program program(vertex, fragment);
  if(!program.linked()){
    cerr << "Program failed to link: " << endl
         << program.get_error_msg() << endl;
    return 1;
  }


  // Adding triangle data to the GPU
  GLuint triangle_vao, triangle_vertex_buffer, triangle_color_buffer;
  glGenVertexArrays(1, &triangle_vao);
  glGenBuffers(1, &triangle_vertex_buffer);
  glGenBuffers(1, &triangle_color_buffer);

  glBindVertexArray(triangle_vao);

  glBindBuffer(GL_ARRAY_BUFFER, triangle_vertex_buffer);
  glBufferData(GL_ARRAY_BUFFER, sizeof(triangle_vertices), triangle_vertices, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

  glBindBuffer(GL_ARRAY_BUFFER, triangle_color_buffer);
  glBufferData(GL_ARRAY_BUFFER, sizeof(triangle_colors), triangle_colors, GL_STATIC_DRAW);

  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);

  glBindVertexArray(0);

  // Adding cube data to the GPU
  GLuint cube_vao, cube_vertex_buffer, cube_indices_buffer;
  glGenVertexArrays(1, &cube_vao);
  glGenBuffers(1, &cube_vertex_buffer);
  glGenBuffers(1, &cube_indices_buffer);

  glBindVertexArray(cube_vao);

  glBindBuffer(GL_ARRAY_BUFFER, cube_vertex_buffer);
  glBufferData(GL_ARRAY_BUFFER, sizeof(cube_vertices), cube_vertices, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cube_indices_buffer);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cube_indices), cube_indices, GL_STATIC_DRAW);

  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);

  glBindVertexArray(0);



  GLuint time_uniform = glGetUniformLocation(program.get_program(), "pos");
  GLuint mvp_uniform = glGetUniformLocation(program.get_program(), "mvp");

  glm::mat4 model;

  glm::mat4 view;
  glm::mat4 projection;
;
  glClearColor(0.2, 0.2, 0.2, 1.0);


  float t = 0;

  while( !glfwWindowShouldClose(window) ){
    int width, height;
    glfwGetWindowSize(window, &width, &height);
    glClear(GL_COLOR_BUFFER_BIT);

    projection = glm::perspective(glm::radians(45.0f), (float)width / (float)height, 0.1f, 100.0f);

    model = glm::rotate(glm::radians(t), glm::vec3(1.0f, 0.0f, 0.0f));
    model = glm::translate(glm::vec3(0,0,-5.0)) * model;

    program.use();
    glUniform1f(time_uniform, sin(t));
    glUniformMatrix4fv(mvp_uniform, 1, GL_FALSE, glm::value_ptr(projection * model));

    //glBindVertexArray(triangle_vao);
    //glDrawArrays(GL_TRIANGLES, 0, 3);
    glBindVertexArray(cube_vao);
    glDrawElements(GL_TRIANGLES, sizeof(cube_indices), GL_UNSIGNED_INT, nullptr);



    glfwSwapBuffers(window);
    glfwPollEvents();
    t += 0.1;
  }

  glfwTerminate();
}
