#version 330 core

layout (location = 0) in vec3 vert;
layout (location = 1) in vec3 color;
out vec4 vertex_color;

out float z;
uniform float pos;
uniform mat4 mvp;


void main(){
  vertex_color = vec4(color,1.0);

  vec4 pos_t = mvp * vec4(vert, 1.0);
  z = pos_t.z;
  gl_Position = pos_t;
}
