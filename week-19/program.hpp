#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "shader.hpp"

class Program{
private:
  GLint program_id_;
  bool  linked_ = true;
  string error_msg_;

public:

  Program(Shader vertex, Shader fragment);

  GLint get_program();
  void use();

  bool linked();
  string get_error_msg();
};
