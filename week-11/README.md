# Extrauppgift vecka 11

Tills nästa vecka är extrauppgiften att implementera sorteringsalgoritmen "Merge sort"
https://en.wikipedia.org/wiki/Merge_sort

Jag tänkte hålla en liten mini tävling om vem som lyckas implementera den
snabbaste versionen. Om du vill vara med i tävlingen så måste ni implementera
algoritmen i C++. Skapa en fil som heter `mergesort.cpp`. I denna ska det finnas
en funktion vid namn `mergesort` som jag kommer kalla på när jag gör
testerna. Det finns en mall i detta repo som ni kan använda för att skriva kod.


Tävlingsreglerna är som följande. Jag kommer att skaffa ett gäng
testfall. Jag gör ett program som kommer att köra en timer kring eran
implementation sen utför den tester som ser till att sorteringen är
korrekt. Kraschar programmet eller sorteringen är felaktig så diskas
man direkt! Gör man inte mergesort så diskas man också!

Mitt test kommer köra varje testfall typ 10 000 gånger så att det
blir jämnt.

Av godtyckliga anledningar skall denna algoritm inte modifiera
original vektorn som kommer in som argument.

Kom ihåg att den ska kunna sortera alla typer av vektorer!

För att testa eran implementation kan ni pröva den på det här kattis problemet:
https://kth.kattis.com/problems/kth.alginda.quicksort

Det står inte hur indata är definierat där men första siffran är antalet siffror
och sen kommer (separerat med mellanslag) siffrorna som ska sorteras.
