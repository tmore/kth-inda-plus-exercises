import Data.Map (Map)
import qualified Data.Map as Map

import Data.List (intercalate)

type Scope = Map Symbol SEXP

data Literal = Char
             | I Integer
             | F Float
             deriving (Show, Read, Eq)

newtype Symbol = Sym String
     deriving (Show, Read, Eq, Ord)

data SEXP = List [SEXP]
         | Symbol Symbol
         | Literal Literal
         | Lambda Scope [Symbol] [SEXP]
         | PrimFunction (Scope -> SEXP -> (SEXP, Scope))

sexpToString :: SEXP -> String
sexpToString (List sexp) = "(" ++ intercalate " " (map sexpToString sexp) ++ ")"
sexpToString (Symbol (Sym s)) = "" ++ s
sexpToString (Literal (I n)) = show n
sexpToString (Literal (F n)) = show n
sexpToString (Lambda _ params body) =
  concat [ "(lambda "
         , intercalate " " (map (\ (Sym s) -> s) params)
         , " "
         , intercalate " " (map sexpToString body)
         ,  ")"
         ]
sexpToString (PrimFunction _) = "<prim-func>"

define :: Scope -> SEXP -> (SEXP, Scope)
define s (List [(Symbol sym), value]) =
  let (val, s') = eval s value
  in (val, Map.insert sym val s')
define s _ = error "Invalid arguments passed to define"


sexpAddition :: Scope -> SEXP -> (SEXP, Scope)
sexpAddition s (List args) =
  foldr (\ sexp ((Literal (I acc)), s') ->
           let (val, s'') = eval s' sexp
           in case val of
                Literal (I n) -> (Literal (I (acc + n)), s'')
                _ -> error "Non integer given to sexp addition"
        ) (Literal (I 0), s) args
sexpAddition _ args = error $ "Addition can't handle those arguments" ++ sexpToString args

apply :: Scope -> SEXP -> (SEXP, Scope)
apply s (List (PrimFunction f : rest)) = f s (List rest)
apply s (List (Lambda parentScope args body : rest)) =
  let (evaled, newScope) = foldr (\ sexp (vals,s') -> let (val, s'') = eval s' sexp
                                                     in (val : vals, s'')) ([],s) rest
      lambdaScope = foldr (\ (param, val) s' -> Map.insert param val s') parentScope (zip args evaled)
      (returnValue, _) = foldr (\ sexp (_, s') -> eval s' sexp
                               ) (List [], lambdaScope) body
  in (returnValue, newScope)
apply s l@(List (Symbol sym : rest)) = eval s l
apply _ _ = error "Cant apply anything to that"

lambda :: Scope -> SEXP -> (SEXP, Scope)
lambda s (List (List params: body)) = (Lambda s (extractSymbols params) body, s)
   where extractSymbols [] = []
         extractSymbols ((Symbol s):xs) = s : extractSymbols xs
         extractSymbols _ = error "Non symbol in parameter list"
lambda _ _ = error "Lambda must be given a parameter list"

eval :: Scope -> SEXP -> (SEXP, Scope)
eval s (Literal a) = (Literal a, s)
eval s (Symbol sym) =
  case Map.lookup sym s of
    Nothing -> error $ "Symbol not in scope: " ++ show sym
    Just v -> (v, s)
eval s (List (x:xs)) =
  let (x', s') = eval s x
  in apply s' (List (x':xs))
eval _ _ = error "Cant evaluate that expression"

globalScope :: Scope
globalScope = Map.fromList [ (Sym "+", PrimFunction sexpAddition)
                           , (Sym "define", PrimFunction define)
                           , (Sym "lambda", PrimFunction lambda)
                           , (Sym "apply", PrimFunction apply)
                           ]
