# Extrauppgift för vecka 12
De som gör denna uppgift slipper göra följande uppgifter
- 1
- 2
- 7

## Extra uppgiften
Så extra uppgiften ni ska göra denna vecka är att lösa "subset sum"
problemet. Det är en de enklare att förstå sig på problemen som är
ligger i NP och är vad som kallas för NP-fullständigt.

Ni kan läsa mer om problemet på [Wikipedia](https://en.wikipedia.org/wiki/Subset_sum_problem)

Jag vill att ni skapar ett program som först tar in först en siffra, k
vilken summa som den ska hitta sedan en ny rad sedan en lista med
heltalssiffrorna. Utdatan till programmet är en rad med siffrorna vars
summa blir k.

Indata:
```
17
7 2 1 5 1 20 7
```
Utdata:

```
7 7 2 1
```

Ni får lösa problemet i vilket programmeringsspråk som ni vill vilken
metod som helst. För de som vill ha en utmaning pröva gärna på
dynamisk programmering.

När ni är klara med lösningen vill jag att ni skriver ner vilken
komplexitet som problemet har och en argumentation för varför. Ni
behöver inte vara super formella men det ska vara möjligt att följa.
